// text generated with http://baconipsum.com/?paras=5&type=meat-and-filler&start-with-lorem=1

if (TopStorySlides.find().count() === 0) {
  TopStorySlides.insert({
    image_file: '/img/FP/1.jpg',
    date:new Date(),
    alt_text:'First slide',
    parent: ''
  });

  TopStorySlides.insert({
    image_file: '/img/FP/2.jpg',
    date:new Date(),
    alt_text:'Second slide',
    parent: ''
  });

  TopStorySlides.insert({
    image_file: '/img/FP/3.jpg',
    date:new Date(),
    alt_text:'Third slide',
    parent:''
  });

}

if (TopStories.find().count() === 0) {
  TopStories.insert({
    title: 'Gladiator School Discovered',
    author: 'admin01',
    slideshow: [
        {slide: "/img/FP/1.jpg" },
        {slide: "/img/FP/2.jpg"  },
        {slide: "/img/FP/3.jpg"  } 
        ],
    thumb: '/img/39.jpg', 
    tags: [
        {tag: 'business'}, 
        {tag: 'politics'},
        {tag: 'sports'},
        {tag: 'technology'}
        ],
    url: '/stories/:_id',
    story_desc: 'Earth sensing technology used to discover ancient gladiator school!!!',
    in_main_slider: true,
    in_news_carousel: false,
    date:new Date(),
    related_content:[{info_graphic: '/img/FP_infographic/1.png'},{video:'//www.youtube.com/embed/CWCwnkdPPCc'},{video_width:'95%'},{video_height:'350'},{video_title:'Gladiator School'},{comic_slideshow:['/img/comic/1.jpg','/img/comic/2.jpg','/img/comic/3.jpg','/img/comic/4.jpg']}],
    story_text:'Outside Vienna an ancient gladiator school has been discovered. It was mapped using special earth-sensing technology and then illustrated to give us an idea of what the grounds looked like. Archaeologists have been able to determine where the gladiators lived, trained and fought. The structure was like a fortress and the gladiators lived like prisoners. They were considered valuable but kept for entertainment purposes only. To be a truly skilled gladiator and therefore offer more entertainment value, they practiced every day. Life as a gladiator was very tough.'
  });
}
if (Stories.find().count() === 0) {
  Stories.insert({
    title: 'Gladiator School Discovered',
    author: 'admin01',
    main_photo: '/img/FP/1.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'Earth sensing technology used to discover ancient gladiator school!!!',
    in_main_slider: true,
    in_news_carousel: false,
    date:new Date(),
    related_content:[{info_graphic: '/img/FP_infographic/1.png'},{video:'//www.youtube.com/embed/CWCwnkdPPCc'},{video_width:'95%'},{video_height:'350'},{video_title:'Gladiator School'},{comic_slideshow:['/img/comic/1.jpg','/img/comic/2.jpg','/img/comic/3.jpg','/img/comic/4.jpg']}],
    story_text:'Outside Vienna an ancient gladiator school has been discovered. It was mapped using special earth-sensing technology and then illustrated to give us an idea of what the grounds looked like. Archaeologists have been able to determine where the gladiators lived, trained and fought. The structure was like a fortress and the gladiators lived like prisoners. They were considered valuable but kept for entertainment purposes only. To be a truly skilled gladiator and therefore offer more entertainment value, they practiced every day. Life as a gladiator was very tough.'
  });

  Stories.insert({
    title: 'Gladiator School Discovered',
    author: 'admin01',
    main_photo: '/img/FP/2.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'Earth sensing technology used to discover ancient gladiator school!!!',
    in_main_slider: true,
    in_news_carousel: false,
    date:new Date(),
    related_content:[{info_graphic: '/img/FP_infographic/1.png'},{video:'//www.youtube.com/embed/CWCwnkdPPCc'},{video_width:'95%'},{video_height:'350'},{video_title:'Gladiator School'},{comic_slideshow:['/img/comic/1.jpg','/img/comic/2.jpg','/img/comic/3.jpg','/img/comic/4.jpg']}],
    story_text:'Outside Vienna an ancient gladiator school has been discovered. It was mapped using special earth-sensing technology and then illustrated to give us an idea of what the grounds looked like. Archaeologists have been able to determine where the gladiators lived, trained and fought. The structure was like a fortress and the gladiators lived like prisoners. They were considered valuable but kept for entertainment purposes only. To be a truly skilled gladiator and therefore offer more entertainment value, they practiced every day. Life as a gladiator was very tough.'
  });

  Stories.insert({
    title: 'Gladiator School Discovered',
    author: 'admin01',
    main_photo: '/img/FP/3.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'Earth sensing technology used to discover ancient gladiator school!!!',
    in_main_slider: true,
    in_news_carousel: false,
    date:new Date(),
    related_content:[{info_graphic: '/img/FP_infographic/1.png'},{video:'//www.youtube.com/embed/CWCwnkdPPCc'},{video_width:'95%'},{video_height:'350'},{video_title:'Gladiator School'},{comic_slideshow:['/img/comic/1.jpg','/img/comic/2.jpg','/img/comic/3.jpg','/img/comic/4.jpg']}],
    story_text:'Outside Vienna an ancient gladiator school has been discovered. It was mapped using special earth-sensing technology and then illustrated to give us an idea of what the grounds looked like. Archaeologists have been able to determine where the gladiators lived, trained and fought. The structure was like a fortress and the gladiators lived like prisoners. They were considered valuable but kept for entertainment purposes only. To be a truly skilled gladiator and therefore offer more entertainment value, they practiced every day. Life as a gladiator was very tough.'
  });

  Stories.insert({
    title: 'Great Story 02',
    author: 'admin02',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 03',
    author: 'admin03',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

    Stories.insert({
    title: 'Great Story 04',
    author: 'admin01',
    main_photo: '/img/12.jpg',
    thumb: '/img/12.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id', 
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: true,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 05',
    author: 'admin02',
    main_photo: '/img/6.jpg',
    thumb: '/img/6.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: true,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 06',
    author: 'admin03',
    main_photo: '/img/1.jpg',
    thumb: '/img/1.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: true,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

    Stories.insert({
    title: 'Great Story 07',
    author: 'admin01',
    main_photo: '/img/15.jpg',
    thumb: '/img/15.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: true,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 08',
    author: 'admin02',
    main_photo: '/img/2.jpg',
    thumb: '/img/2.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: true,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 09',
    author: 'admin03',
    main_photo: '/img/3.jpg',
    thumb: '/img/3.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: true,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });
    Stories.insert({
    title: 'Great Story 10',
    author: 'admin01',
    main_photo: '/img/14.jpg',
    thumb: '/img/14.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: true,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 11',
    author: 'admin02',
    main_photo: '/img/7.jpg',
    thumb: '/img/7.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: true,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 12',
    author: 'admin03',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });
    Stories.insert({
    title: 'Great Story 13',
    author: 'admin01',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 14',
    author: 'admin02',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 15',
    author: 'admin03',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });
    Stories.insert({
    title: 'Great Story 16',
    author: 'admin01',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 17',
    author: 'admin02',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 18',
    author: 'admin03',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 19',
    author: 'admin03',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 20',
    author: 'admin03',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'

  });

  Stories.insert({
    title: 'Great Story 21',
    author: 'admin03',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 22',
    author: 'admin03',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });


}

if (Issues.find().count() === 0) {
  Issues.insert({
  	number: '01',
    title: 'Test Issue 01',
    author: 'Sacha Greif',
    stories: ['story01_id', 'story02_id', 'story03_id', 'story04_id', 'story05_id', 'story06_id', 'story07_id'],
    url: '/issues/:_id', 
    games: ['wordsearch']
  });

  Issues.insert({
  	number: '02',
    title: 'Test Issue 02',
    author: 'Tom Coleman ',
    stories: ['story08_id', 'story09_id', 'story11_id', 'story12_id', 'story13_id', 'story14_id', 'story15_id'],
    url: '/issues/:_id', 
    games: ['wordsearch']
  });

  Issues.insert({
  	number: '03',
    title: 'Test Issue 03',
    author: 'Tom Coleman',
    stories: ['story16_id', 'story17_id', 'story18_id', 'story19_id', 'story20_id', 'story21_id', 'story22_id'],
    url: '/issues/:_id', 
    games: ['wordsearch']
  });
}