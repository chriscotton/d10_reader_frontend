# collections.coffee
@Stories = new Meteor.Collection 'stories'
@Issues = new Meteor.Collection 'issues'
@TopStories = new Meteor.Collection 'topstories'
@TopStorySlides = new Meteor.Collection 'top_story_slides'
