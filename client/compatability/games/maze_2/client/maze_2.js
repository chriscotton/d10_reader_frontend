
/**
 * maze2.js
 *
 * Kids Dailies Maze JS.
 *
 * @author Adrian Iacob <adrian@bmcd.co>
 * @author Nitin N <nitin@bmcd.co>
 * @author Brent McDowell <brent@bmcd.co>
 * @author Bruce Huang <bruce@bmcd.co>
 * @version 0.1
 * @package games
 */
if (Meteor.isClient) {

 // this should come from a game collection..
var maze_2_img_data = { "data" : { "rad_array" : [ 30,
          30,
          30,
        ],
      "seconds" : 120,
      "x_array" : [ 445,
          504,
          621,
     
        ],
      "y_array" : [ 460,
          412,
          400,
        ],
      "right_choice": 1,
      "image_path" : "/img/maze2.jpg", //can be internet available image path
    }
};

var total_seconds = maze_2_img_data.data.seconds + 1;
var x_array = maze_2_img_data.data.x_array;     
var y_array = maze_2_img_data.data.y_array;
var rad_array =maze_2_img_data.data.rad_array;
var right_choice_index = maze_2_img_data.data.right_choice || 0;
var image_path = maze_2_img_data.data.image_path || "/img/maze2.jpg";

  var maze_2_canvas;
  var context;
  var count = 0;

  window.onload = function() {
    

  };
  function oCirkus(centerX,centerY, radius, color ) {

        context.beginPath();
        context.arc(centerX, centerY, radius, 0, 2*Math.PI, false);
        context.lineWidth = 2;
        context.strokeStyle = color;
        context.stroke();
    }

  o_position = function(e) {
    if(count > y_array.length - 1)
    {
      return false;
    }
    var x_diff;
    var y_diff;
    var distance0, distance1;
    var width=document.getElementById('maze_2_canvas').width;
    var offset_left =  document.getElementById('maze_2_canvas').offsetLeft;
    var offset_top =  document.getElementById('maze_2_canvas').offsetTop;

    e = arguments.callee.caller.arguments[0] || window.event

    if (e.pageX == null && e.clientX != null ) { 
      var html = document.documentElement
      var body = document.body
  
      e.pageX = e.clientX + (html && html.scrollLeft || body && body.scrollLeft || 0) - (html.clientLeft || 0)
      e.pageY = e.clientY + (html && html.scrollTop || body && body.scrollTop || 0) - (html.clientTop || 0)
    }
    x=e.pageX;
    y=e.pageY;
    x = x - offset_left;
    y = y - offset_top;

    if(x_array.length == y_array.length) {
      length = x_array.length;
      for (var i = 0; i < length; i++) {
        x_diff=x_array[i]-x;
        y_diff=y_array[i]-y;
        x_diff1=x_diff+width/2;
        distance0=Math.sqrt(x_diff*x_diff+y_diff*y_diff);
        distance1=Math.sqrt(x_diff1*x_diff1+y_diff*y_diff);
        
        if ( (distance0 < rad_array[i])||(distance1 < rad_array[i]))  {
          count++;
          if(i === right_choice_index)
          { 
            oCirkus(x_array[i],y_array[i],rad_array[i],'green');
            alert('You win!');
          }
          else
          {
            oCirkus(x_array[i],y_array[i],rad_array[i],'red');
            alert('Try Again :-)');
          }
          //here may add code to load next image
        }
      }
    }
  }


  function drawMaze(mazeFile, startingX, startingY) {


    // Load the maze picture.
    var imgMaze = new Image();
    imgMaze.onload = function() {
      // Resize the maze_2_canvas to match the maze picture.
      maze_2_canvas.width = imgMaze.width;
      maze_2_canvas.height = imgMaze.height;

      // Draw the maze.
      context.drawImage(imgMaze, 0,0);

    };
    imgMaze.src = mazeFile;
  }

  

  Template.maze_2.rendered = function () {
   // Set up the maze_2_canvas.
    maze_2_canvas = document.getElementById("maze_2_canvas");
    context = maze_2_canvas.getContext("2d");
    count = 0;
    // Draw the maze background.
    drawMaze(image_path, 268, 5);
  };

  Template.maze_2.events({
    'click #maze_2_canvas': function(evt) {
        o_position(evt);
    }
  });

}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}
