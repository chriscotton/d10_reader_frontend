if Meteor.isClient
	Router.map ->

		@.route 'home',
			path: '/home'
			template: 'widget_homepage'
			layoutTemplate: 'main_layout'
			onBeforeAction: ->
				console.log 'routing to /'
			onAfterAction: ->
				Session.set 'isHome', true

		@.route '/',
			path: '/'
			template: 'widget_homepage'
			layoutTemplate: 'main_layout'
			onBeforeAction: ->
				console.log 'routing to /'
			onAfterAction: ->
				Session.set 'isHome', true

		@.route 'story_list',
			path: '/story_list'
			template: 'story_list'
			layoutTemplate: 'main_layout'
			onBeforeAction: ->
				console.log 'routing to /story_list'
			onAfterAction: ->
				Session.set 'isHome', false

		@.route 'info_graphic',
			path: '/info_graphic'
			template: 'info_graphic'
			layoutTemplate: 'main_layout'
			onBeforeAction: ->
				console.log 'routing to /info_graphic'
			onAfterAction: ->
				Session.set 'isHome', false

		@.route 'image_scramble',
			path: '/image_scramble'
			template: 'image_scramble'
			layoutTemplate: 'main_layout'
			onBeforeAction: ->
				console.log 'routing to /image_scramble'
			onAfterAction: ->
				Session.set 'isHome', false

		@.route 'maze_2',
			path: '/maze_2'
			template: 'maze_2'
			layoutTemplate: 'main_layout'
			onBeforeAction: ->
				console.log 'routing to /maze_2'
			onAfterAction: ->
				Session.set 'isHome', false

		@.route 'maze',
			path: '/maze'
			template: 'maze'
			layoutTemplate: 'main_layout'
			onBeforeAction: ->
				console.log 'routing to /maze'
			onAfterAction: ->
				Session.set 'isHome', false

		@.route 'spot_the_difference',
			path: '/spot_the_difference'
			template: 'spot_the_difference'
			layoutTemplate: 'main_layout'
			onBeforeAction: ->
				console.log 'routing to /spot_the_difference'
			onAfterAction: ->
				Session.set 'isHome', false

		@.route 'wordsearch',
			path: '/wordsearch'
			template: 'wordsearch'
			layoutTemplate: 'main_layout'
			onBeforeAction: ->
				console.log 'routing to /wordsearch'
			onAfterAction: ->
				Session.set 'isHome', false

		@.route 'crossword',
			path: '/crossword'
			template: 'crossword'
			layoutTemplate: 'main_layout'
			onBeforeAction: ->
				console.log 'routing to /crossword'
			onAfterAction: ->
				Session.set 'isHome', false

		@.route 'tic-tac-toe',
			path: '/tic-tac-toe'
			template: 'tic-tac-toe'
			layoutTemplate: 'main_layout'
			onBeforeAction: ->
				console.log 'routing to /tic-tac-toe'
			onAfterAction: ->
				Session.set 'isHome', false

		@.route 'single_story',
			path: '/stories/:_id'
			template: 'single_story01'
			layoutTemplate: 'main_layout'
			onBeforeAction: ->
				console.log 'routing to single_story'
			waitOn: -> 
				return Meteor.subscribe('books', this.params._id)
    		data: (_id)->
    			return books.findOne(this.params._id)
			onAfterAction: (_id) ->
						# Session.set 'currentPage', this.id
						Session.set 'currentStoryId', _id					
						Session.set 'isHome', false
						console.log 'our story id is ' + Session.get 'currentStoryId'
						console.log 'home route ' + Session.get 'isHome'
	
	# Router.configure layout: 'main_layout'