if (Meteor.isClient) {
	
	Template.related_video_widget.helpers({ // how to make this faster!!!! 
		currentStory: function() {
			return Stories.findOne({_id:Session.get('currentStoryId')});
		},

		videoTitle : function() {
			return Stories.findOne({_id:Session.get('currentStoryId')}).related_content.video_title;
		}

	});
}
